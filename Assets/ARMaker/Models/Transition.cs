﻿using UnityEngine;

namespace ARMaker.Models
{
    public class Transition : ScriptableObject
    {
        public Event Event;
        public BaseState OldState;
        public BaseState NewState;

        public MonoBehaviour Action;

        private Transition()
        {
            
        }

        public static Transition Create(BaseState oldState, BaseState newState, Event eventObj)
        {
            var transition = ScriptableObject.CreateInstance<Transition>();
            transition.Event = eventObj;
            transition.OldState = oldState;
            transition.NewState = newState;
            return transition;
        }
    }
}
