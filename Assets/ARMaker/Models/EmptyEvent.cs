﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ARMaker.Attributes;
using UnityEngine;

namespace ARMaker.Models
{
    [HideEventType]
    public class EmptyEvent : Event
    {
        public static EmptyEvent Create()
        {
            var emptyEvent = ScriptableObject.CreateInstance<EmptyEvent>();
            return emptyEvent;
        }

        public override bool ArgsEquals(EventArgs args)
        {
            return Args == args;
        }
    }
}
