﻿using System;
using System.Linq;
using UnityEngine;

namespace ARMaker.Models
{
    public class StartState : BaseState
    {
        [SerializeField]
        private ComplexState _parentState;

        protected StartState()
        {
            
        }

        public static StartState Create(Rect rect, ComplexState parentState = null)
        {
            var startState = ScriptableObject.CreateInstance<StartState>();
            startState.Rect = rect;
            startState.AddEvent(EmptyEvent.Create());
            startState.ParentState = parentState;
            return startState;
        }

        public new string Name
        {
            get { return "Start"; }
        }

        public Event Event
        {
            get
            {
                if (Events == null || !Events.Any())
                    throw new ArgumentNullException("Start state doesn't have events");
                return Events[0];
            }
        }

        public ComplexState ParentState
        {
            get { return _parentState; }
            set { _parentState = value; }
        }
    }
}
