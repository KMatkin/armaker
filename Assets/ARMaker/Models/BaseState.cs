﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using UnityEngine;

namespace ARMaker.Models
{
    public class BaseState : ScriptableObject
    {
        [SerializeField] private string _name;
        [SerializeField] private List<Event> _events;
        [SerializeField] public Rect Rect;

        protected BaseState()
        {
               
        }

        public void Destroy()
        {
            if (_events != null)
            {
                foreach (var eventObj in Events)
                {
                    DestroyImmediate(eventObj);
                }
            }
            DestroyImmediate(this);
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public IList<Event> Events
        {
            get { return _events; }
        }

        public void AddEvent(Event eventObject)
        {
            if (_events == null)
                _events = new List<Event>();
            _events.Add(eventObject);
        }

        public void DeleteEvent(Event eventObject)
        {
            _events.Remove(eventObject);
            ScriptableObject.DestroyImmediate(eventObject);
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
