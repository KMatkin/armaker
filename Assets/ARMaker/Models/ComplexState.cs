﻿using System;
using System.Collections.Generic;
using System.Linq;
using FullSerializer;
using UnityEngine;
using UnityEngine.Events;

namespace ARMaker.Models
{
    public class ComplexState : BaseState
    {
        [SerializeField, HideInInspector] private List<ComplexState> _chidren;
        [SerializeField, HideInInspector] private ComplexState _parentState;
        [SerializeField, HideInInspector] private StartState _startState;

        public UnityEvent onEnter;
        public UnityEvent onExit;
        public UnityEvent stateUpdate;

        public static ComplexState Create(string name, Rect rect, ComplexState parentState = null)
        {
            var complexState = ScriptableObject.CreateInstance<ComplexState>();
            complexState.Name = name;
            complexState.Rect = rect;
            complexState.ParentState = parentState;
            DontDestroyOnLoad(complexState);
            complexState.hideFlags = HideFlags.DontUnloadUnusedAsset;
            return complexState;
        }

        protected ComplexState()
        {
        }

        private void OnDestroy()
        {
            if (_chidren != null)
            {
                foreach (var childState in _chidren)
                {
                    DestroyImmediate(childState);
                }
            }
        }

        public Rect[] InputRects { get; set; }

        public IEnumerable<ComplexState> Children
        {
            get { return _chidren; }
        }

        public ComplexState ParentState
        {
            get { return _parentState; }
            set { _parentState = value; }
        }

        public StartState StartState
        {
            get { return _startState; }
            set { _startState = value; }
        }

        public bool IsSimple
        {
            get { return _chidren == null || (_chidren != null && _chidren.Count == 0); }
        }

        public bool HasChildren
        {
            get { return !ReferenceEquals(Children, null) && Children.Any(); }
        }

        public bool HasParent
        {
            get { return ParentState != null; }
        }

        [fsIgnore]
        public int ZIndex { get; set; }

        public void AddChild(ComplexState state)
        {
            if (_chidren == null)
            {
                _chidren = new List<ComplexState>();
            }
            if (StartState == null)
            {
                CreateNestedStartState();
            }
            _chidren.Add(state);
        }

        private void CreateNestedStartState()
        {
            StartState = StartState.Create(new Rect(0, 0, 70, 20), this);
        }

        public bool RemoveChild(ComplexState state)
        {
            if (_chidren != null)
            {
                return _chidren.Remove(state);
            }
            return false;
        }

        public ComplexState GetMostParentalState()
        {
            if (ParentState != null)
                return ParentState.GetMostParentalState();
            return this;
        }

        public override string ToString()
        {
            return String.Format("{0}:({1})", Name,
                _chidren != null && _chidren.Any()
                    ? _chidren.Select(x => x.ToString()).Aggregate((c, n) => c + "," + n)
                    : "");
        }
    }
}
