﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ARMaker.Models
{
    public abstract class Event : ScriptableObject
    {
        [SerializeField]
        public string Name;
        [SerializeField]
        public EventArgs Args;

        protected Event()
        {
            
        }

        public override string ToString()
        {
            return Name;
        }

        public abstract bool ArgsEquals(EventArgs args);
    }
}
