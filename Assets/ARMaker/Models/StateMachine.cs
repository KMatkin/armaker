﻿using System;
using System.Collections.Generic;
using System.Linq;
using FullSerializer;
using UnityEngine;
using Object = UnityEngine.Object;

namespace ARMaker.Models
{
    [Serializable]
    public class StateMachine
    {
        [fsIgnore] public BaseState currentState;
        [SerializeField] private List<ComplexState> _states;
        [SerializeField] private List<Transition> _transitions;
        [SerializeField] private StartState _startState;

        public IEnumerable<ComplexState> States
        {
            get { return _states; }
        }

        public int StateCount
        {
            get { return _states != null ? _states.Count : 0; }
        }

        public IEnumerable<Transition> Transitions
        {
            get { return _transitions; }
        }

        public StartState Start
        {
            get { return _startState; }
            set { _startState = value; }
        }

        public void AddState(ComplexState newState)
        {
            if (States == null)
                _states = new List<ComplexState>();
            _states.Add(newState);
        }

        public void RemoveState(ComplexState state)
        {
            if (state.Children != null)
            {
                foreach (var childState in state.Children.ToList())
                {
                    RemoveState(childState);
                }
            }
            RemoveTransitions(state);
            _states.Remove(state);
            if (state.ParentState != null)
                state.ParentState.RemoveChild(state);
            ScriptableObject.DestroyImmediate(state);
        }

        public void AddTransition(Transition transition)
        {
            if (_transitions == null)
                _transitions = new List<Transition>();
            _transitions.Add(transition);
        }

        public void RemoveTransition(Transition transition)
        {
            if (_transitions != null)
            {
                _transitions.Remove(transition);
                ScriptableObject.DestroyImmediate(transition);
            }
        }

        public void RemoveTransitions(BaseState state)
        {
            if (_transitions != null)
            {
                var transitionToDelete = _transitions.Where(t => t.OldState == state || t.NewState == state).ToList();
                transitionToDelete.ForEach(RemoveTransition);
            }
        }

        public bool MakeTransition(BaseState from, BaseState to, Event eventObj)
        {
            if (from == null || to == null || eventObj == null)
                throw new ArgumentNullException();
            AddTransition(Transition.Create(from, to, eventObj));
            return true;
        }

        public void RemoveTransitions(BaseState state, Event eventObj)
        {
            if (_transitions != null)
                _transitions.RemoveAll(t => t.OldState == state && t.Event == eventObj);
        }

        public void SetZIndex(ComplexState selectedState, int value)
        {
            var parentState = selectedState.GetMostParentalState();
             _states.ForEach(s => s.ZIndex = 0);
            parentState.ZIndex = value;
        }

        public void DeleteEventState(ComplexState target, Event eventObj)
        {
            if (_transitions != null)
                _transitions.RemoveAll(t => t.Event == eventObj);
            target.DeleteEvent(eventObj);
        }

        public IEnumerable<Transition> TransitionsFromState(BaseState state)
        {
            return _transitions != null 
                ? _transitions.Where(t => t.OldState == state) 
                : Enumerable.Empty<Transition>();
        }

        public void Destroy()
        {
            if (_states != null)
            {
                foreach (var state in _states)
                {
                    if (state != null)
                        state.Destroy();
                }
            }
            if (_startState != null)
                _startState.Destroy();
            if (_transitions != null)
            {
                foreach (var transition in _transitions)
                {
                    Object.DestroyImmediate(transition);
                }
            }
        }
    }
}
