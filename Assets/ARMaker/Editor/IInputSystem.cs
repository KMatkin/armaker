﻿using System;
using UnityEngine;

namespace ARMaker.Editor
{
    interface IInputSystem
    {
        void Setup();
        bool HandleInputEvents(EditorState state);
        bool HandleInputEventsLate(EditorState state);
    }

    public class EditorInputInfo
    {
        public string message;
        public EditorState editorState;
        public UnityEngine.Event inputEvent;
        public Vector2 inputPos;

        public EditorInputInfo(EditorState EditorState)
        {
            message = null;
            editorState = EditorState;
            inputEvent = UnityEngine.Event.current;
            inputPos = inputEvent.mousePosition;
        }

        public EditorInputInfo(string Message, EditorState EditorState)
        {
            message = Message;
            editorState = EditorState;
            inputEvent = UnityEngine.Event.current;
            inputPos = inputEvent.mousePosition;
        }

        internal void SetAsCurrentEnvironment()
        {
            //StateEditorWindow.Editor
        }
    }
}
