﻿using System;
using System.Reflection;
using UnityEngine;

namespace ARMaker.Editor.Attributes
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    public class EventHandlerAttribute : Attribute
    {
        public EventType? handledEvent { get; private set; }
        public int priority { get; private set; }

        /// <summary>
        /// Handle all events of the specified eventType
        /// </summary>
        public EventHandlerAttribute(EventType eventType, int priorityValue)
        {
            handledEvent = eventType;
            priority = priorityValue;
        }

        /// <summary>
        /// Handle all events of the specified eventType
        /// </summary>
        public EventHandlerAttribute(int priorityValue)
        {
            handledEvent = null;
            priority = priorityValue;
        }

        /// <summary>
        /// Handle all events of the specified eventType
        /// </summary>
        public EventHandlerAttribute(EventType eventType)
        {
            handledEvent = eventType;
            priority = 50;
        }

        /// <summary>
        /// Handle all EventTypes
        /// </summary>
        public EventHandlerAttribute()
        {
            handledEvent = null;
        }

        internal static bool AssureValidity(MethodInfo method, EventHandlerAttribute attr)
        {
            if (!method.IsGenericMethod && !method.IsGenericMethodDefinition && (method.ReturnType == null || method.ReturnType == typeof(void)))
            { // Check if the method has the correct signature
                ParameterInfo[] methodParams = method.GetParameters();
                if (methodParams.Length == 1 && methodParams[0].ParameterType == typeof(EditorInputInfo))
                    return true;
                Debug.LogWarning("Method " + method.Name + " has incorrect signature for EventHandlerAttribute!");
            }
            return false;
        }
    }
}
