﻿using System;
using System.Reflection;
using ARMaker.Editor.Utils;
using UnityEngine;

namespace ARMaker.Editor.Attributes
{
    [AttributeUsage(AttributeTargets.Method)]
    public class ContextEntryAttribute : Attribute
    {
        public ContextType contextType { get; private set; }
        public string contextPath { get; private set; }

        public ContextEntryAttribute(ContextType type, string path)
        {
            contextType = type;
            contextPath = path;
        }

        internal static bool AssureValidity(MethodInfo method, ContextEntryAttribute attr)
        {
            if (!method.IsGenericMethod && !method.IsGenericMethodDefinition && (method.ReturnType == null || method.ReturnType == typeof(void)))
            { // Check if the method has the correct signature
                ParameterInfo[] methodParams = method.GetParameters();
                if (methodParams.Length == 1 && methodParams[0].ParameterType == typeof(EditorInputInfo))
                    return true;
                else
                    Debug.LogWarning("Method " + method.Name + " has incorrect signature for ContextAttribute!");
            }
            return false;
        }
    }
}
