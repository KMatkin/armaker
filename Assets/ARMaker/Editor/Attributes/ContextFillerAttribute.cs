﻿using System;
using System.Reflection;
using ARMaker.Editor.Utils;
using UnityEngine;

namespace ARMaker.Editor.Attributes
{
    [AttributeUsage(AttributeTargets.Method)]
    public class ContextFillerAttribute : Attribute
    {
        public ContextType contextType { get; private set; }

        public ContextFillerAttribute(ContextType type)
        {
            contextType = type;
        }

        internal static bool AssureValidity(MethodInfo method, ContextFillerAttribute attr)
        {
            if (!method.IsGenericMethod && !method.IsGenericMethodDefinition && (method.ReturnType == null || method.ReturnType == typeof(void)))
            { // Check if the method has the correct signature
                ParameterInfo[] methodParams = method.GetParameters();
                if (methodParams.Length == 2 && methodParams[0].ParameterType == typeof(EditorInputInfo) && methodParams[1].ParameterType == typeof(GenericMenu))
                    return true;
                else
                    Debug.LogWarning("Method " + method.Name + " has incorrect signature for ContextAttribute!");
            }
            return false;
        }
    }
}
