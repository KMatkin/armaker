﻿using System;
using ARMaker;
using ARMaker.Editor;
using ARMaker.Editor.Utils;
using UnityEditor;
using UnityEngine;
using ARMaker.Scripts;
using ARMaker.Utils;
using NodeEditorFramework.Utilities;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;
using Event = UnityEngine.Event;

public class StateEditorWindow : EditorWindow
{
    public static StateEditorWindow window;

    private static IInputSystem _inputSystem;
    private EditorState prevEditorState;
    public EditorState currentEditorState;
    private StateMachineComponent stateMachineComponent;

    [UnityEditor.MenuItem("Window/ARMaker")]
    static void ShowEditor()
    {
        window = EditorWindow.GetWindow<StateEditorWindow>();
        window.titleContent = new GUIContent("ARMaker");
        ARMaker.Editor.Utils.Resources.Init();
    }

    [UnityEditor.MenuItem("ARMaker/Reset state machine")]
    static void ResetStateMachine()
    {
        StateEditorGUI.NewStateMachine();
    }

    private IInputSystem InputSystem
    {
        get
        {
            if (_inputSystem == null)
            {
                _inputSystem = new InputSystem();
                _inputSystem.Setup();
            }
            return _inputSystem;
        }
    }

    public static StateEditorWindow Editor
    {
        get
        {
            if (window == null)
            {
                window = EditorWindow.GetWindow<StateEditorWindow>();
            }
            return window;
        }
    }

    public static Rect ScreenRect
    {
        get { return new Rect(0, 0, Screen.width, Screen.height); }
    }

    void OnGUI()
    {
#if UNITY_EDITOR
        if (window == null)
        {
            ShowEditor();
            MenuController.Setup();
            ContextMenuController.Setup();
            ConnectController.Setup();
            SetupEditorStates();
        }
        if (currentEditorState == null)
            SetupEditorStates();
        GUIScaleUtility.CheckInit();
        SetupStateMachineComponent();
        if (stateMachineComponent == null)
            return;
        stateMachineComponent.SetupStartState();

        Rect canvasRect = currentEditorState.canvasRect;
        currentEditorState.zoomPanAdjust =  GUIScaleUtility.BeginScale(ref canvasRect, currentEditorState.zoomPos, currentEditorState.zoom, true, false);
        StateEditorGUI.StartGUI();
        if (stateMachineComponent.StateMachine != null)
        {
            StateEditorGUI.DrawStartState(stateMachineComponent.StateMachine);
        }
        InputSystem.HandleInputEvents(currentEditorState);
        if (Event.current.type == EventType.Layout && currentEditorState.selectedState != null)
        {
            stateMachineComponent.StateMachine.SetZIndex(currentEditorState.selectedState, 1000);
        }
        if (stateMachineComponent.StateMachine != null)
        {
            StateEditorGUI.DrawCanvas(stateMachineComponent.StateMachine);
            if (currentEditorState.connectState)
            {
                StateEditorGUI.DrawConnectionCurve();
                Repaint();
            }
        }
        InputSystem.HandleInputEventsLate(currentEditorState);
        StateEditorGUI.EndGUI();
        prevEditorState = currentEditorState;
        if (GUI.changed)
        {
            EditorSceneManagerWrapper.MakeCurrentSceneDirty();
        }
        GUIScaleUtility.EndScale();
#endif
    }

    private void SetupStateMachineComponent()
    {
        stateMachineComponent = StateController.GetStateMachineComponent();
    }

    void SetupEditorStates()
    {
        if (currentEditorState == null)
        {
            currentEditorState = ScriptableObject.CreateInstance<EditorState>();
            prevEditorState = ScriptableObject.CreateInstance<EditorState>();
        }
        currentEditorState.canvasRect = ScreenRect;
        prevEditorState.canvasRect = ScreenRect;
    }
}
