﻿using System.Collections.Generic;
using System.Linq;
using ARMaker.Editor.Utils;
using ARMaker.Models;
using ARMaker.Utils;
using Assets.ARMaker.Editor;
using UnityEngine;
using Event = ARMaker.Models.Event;

namespace ARMaker.Editor
{
    static partial class StateMachineRenderer
    {
        public const int EventBoxWidth = 100;
        public const int BoxPadding = 10;
        public const int MinStateWidth = EventBoxWidth + 50;
        public const int MinStateHeight = 75;
        public const int InputBoxWidth = BoxPadding;
        public const int InputBoxHeight = BoxPadding;

        public static void Render(BaseState state)
        {
            if (ReferenceEquals(state, null))
                return;
            if (state is ComplexState)
                RenderComplexState((ComplexState) state);
            else if (state is StartState)
                RenderStartState((StartState) state);
        }

        private static void RenderStartState(StartState startState)
        {
            if (GUI.Button(startState.ParentState == null ? startState.GetRectOnCanvas() : startState.Rect,
                    "Start state"))
            {
                StateEditorGUI.EventButtonClicked(startState, startState.Event);
            }
        }

        private static void RenderComplexState(ComplexState state)
        {
            Rect stateRect = state.ParentState == null ? state.GetRectOnCanvas() : state.Rect;
            GUI.BeginGroup(stateRect);
            {
                var areaRect = new Rect(0, 0, stateRect.width, stateRect.height);

                var stateRectWithPadding = GeomUtils.GetRectWithPadding(areaRect, BoxPadding);
                GUI.Box(stateRectWithPadding, state.Name);
                {
                    GUILayout.BeginHorizontal();

                    var eventBoxWidth = !state.HasChildren ? stateRectWithPadding.width : EventBoxWidth;
                    // Events
                    GUILayout.BeginArea(new Rect(BoxPadding, BoxPadding+ Utils.Resources.LabelLineHeight, eventBoxWidth, stateRectWithPadding.height));
                    {
                        GUILayout.BeginVertical();
                        GUILayout.Label(Utils.Resources.EventsHeader, GUILayout.Height(Utils.Resources.LabelLineHeight));
                        if (state.Events != null)
                        {
                            int eventCount = state.Events.Count;
                            for (int i = 0; i < eventCount; i++)
                            {
                                var stateEvent = state.Events[i];
                                if (GUILayout.Button(stateEvent.Name, GUILayout.Height(Utils.Resources.ButtonHeight)))
                                {
                                    StateEditorGUI.EventButtonClicked(state, stateEvent);
                                }
                            }
                        }
                        GUILayout.EndVertical();
                    }
                    GUILayout.EndArea();

                    if (state.HasChildren)
                    {
                        // Nested states
                        var nestedEventsRect = GeomUtils.GetRectWithPadding(stateRectWithPadding, EventBoxWidth,
                            Utils.Resources.LabelLineHeight, 0, 0);
                        GUI.Box(nestedEventsRect, Utils.Resources.StatesHeader);
                        if (state.Children != null)
                        {
                            GUI.BeginGroup(nestedEventsRect);
                            Render(state.StartState);
                            foreach (var nestedState in state.Children)
                            {
                                Render(nestedState);
                            }
                            GUI.EndGroup();
                        }
                        GUILayout.EndHorizontal();
                    }

                    var inputRect = new Rect(0, 25, InputBoxWidth, InputBoxHeight);
                    var absRect = GetAbsoluteRect(state);
                    var inputRectAbs = new Rect(inputRect);
                    inputRectAbs.position += absRect.position;
                    inputRectAbs.x -= BoxPadding;
                    inputRectAbs.y -= BoxPadding;
                    if (state.InputRects == null || state.InputRects.Length == 0)
                        state.InputRects = new Rect[2];

                    if (GUI.Button(inputRect, "-"))
                    {
                        StateEditorGUI.StateInputClicked(state);
                    }
                    state.InputRects[0] = inputRectAbs;
                    inputRect.x += absRect.width + BoxPadding;
                    inputRectAbs.x += absRect.width + BoxPadding;
                    state.InputRects[1] = inputRectAbs;

                    if (GUI.Button(inputRect, "-"))
                    {
                        StateEditorGUI.StateInputClicked(state);
                    }

                    // Draw borders if resize
                    if (StateEditorWindow.Editor.currentEditorState.selectedState == state
                        && StateEditorWindow.Editor.currentEditorState.resizingState)
                    {
                        Rect bordersRect = GetBorderRect(stateRectWithPadding, EventHandlers.resizeDirection);
                        GUI.Box(bordersRect, GUIContent.none, Styles.BorderStyle);
                    }
                }
            }

            GUI.EndGroup();
        }

        private static Rect GetBorderRect(Rect stateRectWithPadding, BorderSelection resizeDirection)
        {
            Rect borderRect = stateRectWithPadding;
            var borderWidth = Utils.Resources.ResizeBorderWidth;
            if ((resizeDirection & BorderSelection.Left) != 0)
                borderRect.xMax = borderRect.xMin + borderWidth;
            else if ((resizeDirection & BorderSelection.Right) != 0)
                borderRect.xMin = borderRect.xMax - borderWidth;
            if ((resizeDirection & BorderSelection.Top) != 0)
                borderRect.yMax = borderRect.yMin + borderWidth;
            else if ((resizeDirection & BorderSelection.Bottom) != 0)
                borderRect.yMin = borderRect.yMax - borderWidth;
            return borderRect;
        }

        public static Rect GetAbsoluteRect(ComplexState state)
        {
            if (state == null)
                return Rect.zero;
            if (state.ParentState == null)
                return GeomUtils.GetRectWithPadding(state.Rect, BoxPadding).AddCanvasOffsets();

            var res = new Rect(GetAbsoluteRect(state.ParentState));
            res.xMin += state.Rect.xMin + EventBoxWidth;
            res.yMin += state.Rect.yMin + Utils.Resources.LabelLineHeight;
            res.width = state.Rect.width;
            res.height = state.Rect.height;
            return GeomUtils.GetRectWithPadding(res, BoxPadding);
        }

        public static Rect MakeNestedArea(Rect stateRect)
        {
            var nestedArea = new Rect(stateRect);
            nestedArea.position += new Vector2(EventBoxWidth, Utils.Resources.LabelLineHeight);
            return nestedArea;
        }

        public static Rect GetAbsoluteRect(BaseState state)
        {
            return state.Rect; //TODO:
        }

        public static Rect GetNestedRect(ComplexState state)
        {
            var res = Rect.zero;
            res.width = state.Rect.width - EventBoxWidth - BoxPadding;
            res.height = state.Rect.height - Utils.Resources.LabelLineHeight - BoxPadding;
            return res;
        }

        public static bool CanDrag(ComplexState state, Vector2 newPosition)
        {
            if (state.ParentState == null)
                return true;
            return CanDragNested(state, newPosition);
        }

        private static bool CanDragNested(ComplexState state, Vector2 newPosition)
        {
            var nestedRect = StateMachineRenderer.GetNestedRect(state.ParentState);
            nestedRect.xMax -= state.Rect.width;
            nestedRect.yMin += Utils.Resources.LabelLineHeight;
            nestedRect.yMax -= state.Rect.height;
            return nestedRect.Contains(newPosition);
        }

        public static Rect GetEventRect(BaseState state, Event eventObj)
        {
            if (state is ComplexState)
                return GetEventRect((ComplexState) state, eventObj);
            else if (state is StartState)
                return GetEventRect((StartState)state);
            return Rect.zero;
        }

        private static Rect GetEventRect(StartState state)
        {
            if (state.ParentState == null)
                return state.GetRectOnCanvas();
            else
            {
                var nestedArea = MakeNestedArea(GetAbsoluteRect(state.ParentState));
                nestedArea.position += state.Rect.position;
                nestedArea.width = state.Rect.width;
                nestedArea.height = state.Rect.height;
                return nestedArea;
            }
        }

        public static Rect GetEventRect(ComplexState state, Event eventObj)
        {
            var absoluteRect = GetAbsoluteRect(state);
            absoluteRect.width = state.HasChildren ? EventBoxWidth : absoluteRect.width;
            absoluteRect.yMin += 2 * Utils.Resources.LabelLineHeight 
                + (Utils.Resources.ButtonHeight + Utils.Resources.EventDividerHeight) * (state.Events.IndexOf(eventObj))
                + 2 * Utils.Resources.BorderWidth 
                + Utils.Resources.BorderWidth;
            //absoluteRect.xMin += BoxPadding;
            absoluteRect.height = Utils.Resources.ButtonHeight;
            return absoluteRect;
        }

        public static IEnumerable<Rect> GetInputRect(BaseState state, Event eventObj)
        {
            if (state is ComplexState)
                return GetInputRect(state);
            return Enumerable.Repeat(state.Rect, 1);
        }

        public static IEnumerable<Rect> GetInputRect(BaseState state)
        {
            if (state is ComplexState)
                return GetInputRect((ComplexState)state);
            return Enumerable.Repeat(state.Rect, 1);
        }

        public static IEnumerable<Rect> GetInputRect(ComplexState state)
        {
            return state.InputRects;
        }

        public static float GetMinStateHeight(ComplexState state)
        {
            if (!state.HasChildren)
                return MinStateHeight;
            var childrenRect = state.GetChildrenRect();
            return childrenRect.yMax + Utils.Resources.LabelLineHeight + Utils.Resources.BorderWidth + BoxPadding;
        }

        public static float GetMinStateWidth(ComplexState state)
        {
            if (!state.HasChildren)
                return MinStateWidth;
            var childrenRect = state.GetChildrenRect();
            return childrenRect.xMax + EventBoxWidth + Utils.Resources.BorderWidth + InputBoxWidth;
        }
    }
}
