﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ARMaker.Editor.Utils
{
    static class Resources
    {
        public static string EventsHeader = "Event";
        public static string StatesHeader = "States";
        public static int BorderWidth = 1;
        public static int EventDividerHeight = 3;

        public static int ResizeBorderWidth = 10;


        private static int _labelLineHeight;
        private static int _buttonHeight;

        public static Texture2D startTexture;

        public static int LabelLineHeight
        {
            get
            {
                if (_labelLineHeight == 0)
                    _labelLineHeight = (int)GUI.skin.label.CalcHeight(new GUIContent(""), 1);
                return _labelLineHeight;
            }
        }

        public static int ButtonHeight
        {
            get
            {
                if (_buttonHeight == 0)
                    _buttonHeight = (int)GUI.skin.button.CalcHeight(new GUIContent(string.Empty), 1);
                return _buttonHeight;
            }
        }

        public static void Init()
        {
            if (startTexture == null)
                startTexture = (Texture2D)UnityEngine.Resources.Load("start", typeof(Texture2D));
        }
    }
}
