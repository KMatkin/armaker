﻿using System.Collections.Generic;
using UnityEngine;

namespace ARMaker.Editor.Utils
{
    public class MenuItem
    {
        public string path;
        // -!Separator
        public GUIContent content;
        // -Executable Item
        public PopupMenu.MenuFunction func;
        public PopupMenu.MenuFunctionData funcData;
        public object userData;
        // -Non-executables
        public bool separator = false;
        // --Group
        public bool group = false;
        public Rect groupPos;
        public List<MenuItem> subItems;

        public MenuItem()
        {
            separator = true;
        }

        public MenuItem(string _path, GUIContent _content, bool _group)
        {
            path = _path;
            content = _content;
            group = _group;

            if (group)
                subItems = new List<MenuItem>();
        }

        public MenuItem(string _path, GUIContent _content, PopupMenu.MenuFunction _func)
        {
            path = _path;
            content = _content;
            func = _func;
        }

        public MenuItem(string _path, GUIContent _content, PopupMenu.MenuFunctionData _func, object _userData)
        {
            path = _path;
            content = _content;
            funcData = _func;
            userData = _userData;
        }

        public void Execute()
        {
            if (funcData != null)
                funcData(userData);
            else if (func != null)
                func();
        }
    }
}
