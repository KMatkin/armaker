﻿using ARMaker.Models;
using FullSerializer;

namespace ARMaker.Editor.Utils
{
    public static class JsonHelper
    {
        private static readonly fsSerializer _serializer = new fsSerializer();

        public static string ToJson(StateMachine stateMachine)
        {
            fsData data;
            _serializer.TrySerialize(typeof(StateMachine), stateMachine, out data);
            string jsonstr = fsJsonPrinter.PrettyJson(data);
            return jsonstr;
        }
    }
}
