﻿using UnityEngine;

namespace ARMaker.Editor.Utils
{
    public class GenericMenu
    {
        private static PopupMenu popup;

        public Vector2 Position { get { return popup.Position; } }

        public GenericMenu()
        {
            popup = new PopupMenu();
        }

        public void ShowAsContext()
        {
            popup.Show(UnityEngine.Event.current.mousePosition);
        }

        public void Show(Vector2 pos, float MinWidth = 40)
        {
            popup.Show(pos, MinWidth);
        }

        public void AddItem(GUIContent content, bool on, PopupMenu.MenuFunctionData func, object userData)
        {
            popup.AddItem(content, on, func, userData);
        }

        public void AddItem(GUIContent content, bool on, PopupMenu.MenuFunction func)
        {
            popup.AddItem(content, on, func);
        }

        public void AddSeparator(string path)
        {
            popup.AddSeparator(path);
        }
    }
}
