﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ARMaker.Editor.Utils
{
    static class GraphicsUtils
    {
        public static Texture2D ColorToTexture(int pxSize, Color color)
        {
            Texture2D tex = new Texture2D(pxSize, pxSize);
            for (int x = 0; x < pxSize; x++)
            for (int y = 0; y < pxSize; y++)
                tex.SetPixel(x, y, color);
            tex.Apply();
            return tex;
        }
    }
}
