﻿using UnityEngine;

namespace ARMaker.Editor.Utils
{
    public static class GeomUtils
    {
        public static Rect GetRectWithPadding(Rect rect, int padding)
        {
            return GetRectWithPadding(rect, padding, padding, padding, padding);
        }

        public static Rect GetRectWithPadding(Rect rect, int left, int top, int right, int bottom)
        {
            var rectWithMargins = new Rect(rect);
            rectWithMargins.xMin += left;
            rectWithMargins.yMin += top;
            rectWithMargins.xMax -= right;
            rectWithMargins.yMax -= bottom;
            return rectWithMargins;
        }

        public static void ClipRect(ref Rect childRect, Rect parentRect)
        {
            if (childRect.xMin < parentRect.xMin)
                childRect.xMin = parentRect.xMin;
            if (childRect.yMin < parentRect.yMin)
                childRect.yMin = parentRect.yMin;
            if (childRect.xMax > parentRect.xMax)
                childRect.xMax = parentRect.xMax;
            if (childRect.yMax > parentRect.yMax)
                childRect.yMax = parentRect.yMax;
        }
    }
}
