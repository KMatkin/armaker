﻿using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;

namespace ARMaker.Editor.Utils
{
    public static class EditorSceneManagerWrapper
    {
        public static void MakeCurrentSceneDirty()
        {
            EditorSceneManager.MarkSceneDirty(SceneManager.GetActiveScene());
        }
    }
}
