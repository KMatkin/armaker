﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ARMaker.Editor.Utils
{
    public static class RectExtensions
    {
        public static float CenterDistance(this Rect self, Rect other)
        {
            return Vector2.Distance(self.center, other.center);
        }
    }
}
