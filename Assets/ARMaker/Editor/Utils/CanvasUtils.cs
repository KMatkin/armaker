﻿using System;
using System.Collections.Generic;
using ARMaker.Models;
using ARMaker.Scripts;
using Assets.ARMaker.Scripts;
using UnityEngine;

namespace ARMaker.Editor.Utils
{
    static class CanvasUtils
    {
        public static ComplexState StateAtPosition(Vector2 pos)
        {
            var stateMachine = StateController.GetStateMachine();
            if (stateMachine == null)
                return null;
            return StateAtPosition(pos, stateMachine.States);
        }

        private static ComplexState StateAtPosition(Vector2 pos, IEnumerable<ComplexState> states)
        {
            if (states == null)
                return null;
            foreach (var state in states)
            {
                var absoluteRect = StateMachineRenderer.GetAbsoluteRect(state);
                if (absoluteRect.Contains(pos))
                {
                    var childState = StateAtPosition(pos, state.Children);
                    return childState == null ? state : childState;
                }
            }
            return null;
        }

        public static bool CheckBorderSelection(ComplexState complexState, Vector2 mousePos, out BorderSelection resizeDirection)
        {
            resizeDirection = BorderSelection.None;
            var absoluteRect = StateMachineRenderer.GetAbsoluteRect(complexState);
            if (!absoluteRect.Contains(mousePos))
                return false;

            var borderWidth = Resources.ResizeBorderWidth;
            var min = new Vector2(absoluteRect.xMin + borderWidth, absoluteRect.yMax - borderWidth);
            var max = new Vector2(absoluteRect.xMax - borderWidth, absoluteRect.yMin + borderWidth);

            if (mousePos.x < min.x)
                resizeDirection |= BorderSelection.Left;
            else if (mousePos.x > max.x)
                resizeDirection |= BorderSelection.Right;
            if (mousePos.y < max.y)
                resizeDirection |= BorderSelection.Top;
            else if (mousePos.y > min.y)
                resizeDirection |= BorderSelection.Bottom;

            return resizeDirection != BorderSelection.None;
        }

        
    }

    [Flags]
    enum BorderSelection
    {
        None = 0,
        Left = 1,
        Right = 2,
        Top = 4,
        Bottom = 8
    };
}
