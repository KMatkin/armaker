﻿using System.Collections.Generic;
using System.Linq;
using ARMaker.Editor.Utils;
using ARMaker.Models;
using ARMaker.Utils;
using UnityEngine;

namespace ARMaker.Editor
{
    static partial class StateMachineRenderer
    {
        public static void Render(Transition transition)
        {
            var eventRect = GetEventRect(transition.OldState, transition.Event);
            var inputRects = GetInputRect(transition.NewState, transition.Event);

            var niceRect = GetNiceInput(inputRects, eventRect);
            if (niceRect.center.x < eventRect.center.x)
            {
                DrawUtils.DrawNodeCurve(niceRect, eventRect);
            }
            else
            {
                DrawUtils.DrawNodeCurve(eventRect, niceRect);
            }
        }

        private static Rect GetNiceInput(IEnumerable<Rect> rect, Rect eventRect)
        {
            return rect.OrderBy(r => r.CenterDistance(eventRect)).FirstOrDefault();
        }
    }
}
