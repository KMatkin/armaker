﻿using UnityEngine;

namespace ARMaker.Editor.Utils
{
    public static class OverlayGUI
    {
        public static PopupMenu CurrentPopup;

        public static bool HasPopupControl()
        {
            return CurrentPopup != null;
        }

        public static void StartOverlayGUI()
        {
            if (CurrentPopup != null && UnityEngine.Event.current.type != EventType.Layout && UnityEngine.Event.current.type != EventType.Repaint)
                CurrentPopup.Draw();
        }

        public static void EndOverlayGUI()
        {
            if (CurrentPopup != null && (UnityEngine.Event.current.type == EventType.Layout || UnityEngine.Event.current.type == EventType.Repaint))
                CurrentPopup.Draw();
        }
    }
}
