﻿using System;
using ARMaker.Models;
using UnityEngine;

namespace ARMaker.Editor.Utils
{
    public static class StateExtensions
    {
        public static Rect GetRectOnCanvas(this BaseState state)
        {
            Rect stateRect = state.Rect;
            stateRect.position += StateEditorWindow.Editor.currentEditorState.panOffset + StateEditorWindow.Editor.currentEditorState.zoomPanAdjust;
            return stateRect;
        }

        public static Rect AddCanvasOffsets(this Rect rect)
        {
            Rect resultRect = rect;
            resultRect.position += StateEditorWindow.Editor.currentEditorState.panOffset + StateEditorWindow.Editor.currentEditorState.zoomPanAdjust;
            return resultRect;
        }

        public static Rect RemoveCanvasOffsets(this Rect rect)
        {
            Rect resultRect = rect;
            resultRect.position = resultRect.position - StateEditorWindow.Editor.currentEditorState.panOffset - StateEditorWindow.Editor.currentEditorState.zoomPanAdjust;
            return resultRect;
        }

        public static Rect GetChildrenRect(this ComplexState state)
        {
            if (!state.HasChildren)
                return Rect.zero;
            Rect nestedRect = Rect.zero;
            float xMin, xMax, yMin, yMax;
            yMin = xMin = Single.NegativeInfinity;
            xMax = yMax = Single.NegativeInfinity;

            foreach (var complexState in state.Children)
            {
                xMax = GetFloatMax(xMax, complexState.Rect.xMax);
                yMax = GetFloatMax(yMax, complexState.Rect.yMax);
                yMin = GetFloatMax(yMin, complexState.Rect.yMin);
                xMin = GetFloatMax(xMin, complexState.Rect.xMin);
            }
            nestedRect.Set(xMin, yMin, xMax - xMin, yMax - yMin);
            return nestedRect;
        }

        private static float GetFloatMax(float a, float b)
        {
            return (float.IsNaN(a)) ? b : (float.IsNaN(b) ? a : Mathf.Max(a, b));
        }
    }
}
