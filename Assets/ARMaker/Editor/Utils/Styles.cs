﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ARMaker.Editor.Utils
{
    static class Styles
    {
        private static GUIStyle _borderStyle;
        private static Texture2D _borderBackgroundTexture;

        public static Texture2D BorderBackgroundTexture
        {
            get
            {
                if (_borderBackgroundTexture == null)
                {
                    _borderBackgroundTexture = GraphicsUtils.ColorToTexture(8, new Color(0.6f, 0.6f, 0.6f, 0.6f));
                }
                return _borderBackgroundTexture;
            }
        }

        public static GUIStyle BorderStyle
        {
            get
            {
                if (_borderStyle == null)
                {
                    _borderStyle = new GUIStyle();
                    _borderStyle.normal.background = BorderBackgroundTexture;
                }
                return _borderStyle;
            }
        }
    }
}
