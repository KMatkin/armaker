﻿using System;
using ARMaker.Models;
using ARMaker.Scripts;
using Event = ARMaker.Models.Event;

namespace ARMaker.Editor
{
    static class ConnectController
    {
        public static BaseState currentState;
        public static Event currentEvent;

        public static void Setup()
        {
            StateEditorGUI.ConnectionClicked -= OnConnectionClicked;
            StateEditorGUI.ConnectionClicked += OnConnectionClicked;
            StateEditorGUI.EventClicked -= OnEventClicked;
            StateEditorGUI.EventClicked += OnEventClicked;
        }

        private static void OnConnectionClicked(ConnectionEventArgs args)
        {
            if (currentState == null)
                return;
            if (StateController.GetStateMachine().MakeTransition(currentState, args.To, currentEvent))
            {
                ClearState();
            }
        }

        private static void OnEventClicked(ConnectionEventArgs args)
        {
            if (args.From == null)
                throw new ArgumentException("State source cannot be null");
            if (currentState != null && args.From != currentState)
                return;
            if (currentState != null)
            {
                ClearState();
            }
            else
            {
                StateController.GetStateMachine().RemoveTransitions(args.From, args.Event);
                StateEditorWindow.Editor.currentEditorState.connectState = true;
                currentState = args.From;
                currentEvent = args.Event;
            }
        }

        private static void ClearState()
        {
            StateEditorWindow.Editor.currentEditorState.connectState = false;
            currentState = null;
            currentEvent = null;
        }
    }
}
