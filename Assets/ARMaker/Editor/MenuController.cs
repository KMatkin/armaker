﻿using System;
using ARMaker.Models;
using Assets.ARMaker.Scripts;
using UnityEngine;

namespace ARMaker.Editor
{
    static class MenuController
    {
        private static bool initialized = false;

        public static void Setup()
        {
            if (!initialized)
            {       
                initialized = true;
            }
        }
    }
}
