﻿using ARMaker.Models;

namespace ARMaker.Editor
{
    class ConnectionEventArgs
    {
        public BaseState From { get; set; }
        public BaseState To { get; set; }
        public Event Event { get; set; }

        public ConnectionEventArgs(BaseState to)
        {
            To = to;
        }

        public ConnectionEventArgs(BaseState fromState, Event eventObj)
        {
            From = fromState;
            Event = eventObj;
        }

        public ConnectionEventArgs(BaseState @from, BaseState to, Event @event)
        {
            From = @from;
            To = to;
            Event = @event;
        }
    }
}
