﻿using System.IO;
using ARMaker.Editor.Utils;
using ARMaker.Scripts;
using UnityEditor;
using UnityEngine;

namespace ARMaker.Editor
{
    public class StateMachineExporter : ScriptableObject
    {
        [UnityEditor.MenuItem("ARMaker/Export/Json")]
        public static void ExportAsJson()
        {
            string fileName = EditorUtility.SaveFilePanel("Export .json file", "", "stateMachine", "json");
            var stateMachine = StateController.GetStateMachine();
            if (!string.IsNullOrEmpty(fileName))
            {
                var json = JsonHelper.ToJson(stateMachine);
                File.WriteAllText(fileName, json);
            }
        }
    }
}
