﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using ARMaker.Editor.Utils;
using UnityEngine;
using ARMaker.Editor.Attributes;

namespace ARMaker.Editor {
    public class InputSystem : IInputSystem
    {
        public static List<KeyValuePair<EventHandlerAttribute, Delegate>> eventHandlers;
        public static List<KeyValuePair<ContextEntryAttribute, PopupMenu.MenuFunctionData>> contextEntries;
        public static List<KeyValuePair<ContextFillerAttribute, Delegate>> contextFillers;

        public void Setup()
        {
            eventHandlers = new List<KeyValuePair<EventHandlerAttribute, Delegate>>();
            contextEntries = new List<KeyValuePair<ContextEntryAttribute, PopupMenu.MenuFunctionData>>();
            IEnumerable<Assembly> scriptAssemblies = AppDomain.CurrentDomain.GetAssemblies().Where(assembly => assembly.FullName.Contains("Assembly"));
            foreach (Assembly assembly in scriptAssemblies)
            {
                foreach (Type type in assembly.GetTypes())
                {
                    foreach (MethodInfo method in type.GetMethods(BindingFlags.FlattenHierarchy | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static))
                    {
                        #region Event-Attributes recognition and storing

                        Delegate actionDelegate = null;
                        foreach (object attr in method.GetCustomAttributes(true))
                        {
                            Type attrType = attr.GetType();
                            if (attrType == typeof(EventHandlerAttribute))
                            {
                                if (EventHandlerAttribute.AssureValidity(method, attr as EventHandlerAttribute))
                                { 
                                    if (actionDelegate == null) actionDelegate = Delegate.CreateDelegate(typeof(Action<EditorInputInfo>), method);
                                    eventHandlers.Add(new KeyValuePair<EventHandlerAttribute, Delegate>(attr as EventHandlerAttribute, actionDelegate));
                                }
                            }
                            else if (attrType == typeof(ContextEntryAttribute))
                            {
                                if (ContextEntryAttribute.AssureValidity(method, attr as ContextEntryAttribute))
                                {
                                    if (actionDelegate == null) actionDelegate = Delegate.CreateDelegate(typeof(Action<EditorInputInfo>), method);
                                    PopupMenu.MenuFunctionData menuFunction = (object callbackObj) =>
                                    {
                                        if (!(callbackObj is EditorInputInfo))
                                            throw new UnityException("Callback Object passed by context is not of type EditorMenuCallback!");
                                        actionDelegate.DynamicInvoke(callbackObj as EditorInputInfo);
                                    };
                                    contextEntries.Add(new KeyValuePair<ContextEntryAttribute, PopupMenu.MenuFunctionData>(attr as ContextEntryAttribute, menuFunction));
                                }
                            }
                            else if (attrType == typeof(ContextFillerAttribute))
                            {
                                if (ContextFillerAttribute.AssureValidity(method, attr as ContextFillerAttribute))
                                {
                                    Delegate methodDel = Delegate.CreateDelegate(typeof(Action<EditorInputInfo, Utils.GenericMenu>), method);
                                    contextFillers.Add(new KeyValuePair<ContextFillerAttribute, Delegate>(attr as ContextFillerAttribute, methodDel));
                                }
                            }
                        }
                        #endregion
                    }
                }
            }
            eventHandlers.Sort((handlerA, handlerB) => handlerA.Key.priority.CompareTo(handlerB.Key.priority));
            
        }

        public bool HandleInputEvents(EditorState state)
        {
            if (ShouldIgnoreInput(state))
                return false;
            EditorInputInfo inputInfo = new EditorInputInfo(state);
            CallEventHandlers(inputInfo, false);
            //CallHotkeys(inputInfo, Event.current.keyCode, Event.current.modifiers);
            return true;
        }

        public bool HandleInputEventsLate(EditorState state)
        {

            if (ShouldIgnoreInput(state))
                return false;
            EditorInputInfo inputInfo = new EditorInputInfo(state);
            CallEventHandlers(inputInfo, true);
            return false;
        }

        private bool ShouldIgnoreInput(EditorState state)
        {
            if (OverlayGUI.HasPopupControl())
                return true;
            return false;
        }

        private void CallEventHandlers(EditorInputInfo inputInfo, bool late)
        {
            object[] parameter = { inputInfo };
            foreach (KeyValuePair<EventHandlerAttribute, Delegate> eventHandler in eventHandlers)
            {
                if ((eventHandler.Key.handledEvent == null || eventHandler.Key.handledEvent == inputInfo.inputEvent.type) &&
                    (late ? eventHandler.Key.priority >= 100 : eventHandler.Key.priority < 100))
                {
                    eventHandler.Value.DynamicInvoke(parameter);
                    if (inputInfo.inputEvent.type == EventType.Used)
                        return;
                }
            }
        }
    }
}
