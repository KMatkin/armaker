﻿using System;
using ARMaker.Models;
using UnityEngine;

public class EditorState : ScriptableObject {
    public bool drawing;

    public ComplexState selectedState;
    [NonSerialized] public ComplexState focusedState;

    [NonSerialized] public bool dragState;
    [NonSerialized] public bool connectState;
    [NonSerialized] public bool resizingState;
    [NonSerialized] public bool panningState;

    
    [NonSerialized] public Vector2 dragMouseStart;
    [NonSerialized] public Vector2 dragObjectStart;
    [NonSerialized] public Vector2 dragOffset;

    public Vector2 panOffset = new Vector2();

    public float zoom = 1;
    public Vector2 zoomPos {get { return StateEditorWindow.ScreenRect.size / 2; } }
    public Vector2 zoomPanAdjust;

    public Rect canvasRect;

    public Vector2 dragObjectPos
    {
        get { return dragObjectStart + dragOffset; }
    }

    public bool StartDrag(Vector2 mousePos, Vector2 objectPos)
    {
        dragMouseStart = mousePos;
        dragObjectStart = objectPos;
        dragOffset = Vector2.zero;
        return true;
    }

    public Vector2 UpdateDrag(Vector2 newDragPos)
    {
        var prevOffset = dragOffset;
        dragOffset = (newDragPos - dragMouseStart);
        return dragOffset - prevOffset;
    }

    public Vector2 EndDrag()
    {
        Vector2 dragPos = dragObjectPos;
        dragOffset = dragMouseStart = dragObjectStart = Vector2.zero;
        return dragPos;
    }
}
