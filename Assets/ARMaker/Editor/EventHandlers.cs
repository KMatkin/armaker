﻿using System;
using System.Collections.Generic;
using ARMaker;
using ARMaker.Editor;
using UnityEngine;
using Event = UnityEngine.Event;
using ARMaker.Editor.Utils;
using ARMaker.Editor.Attributes;
using ARMaker.Models;

namespace Assets.ARMaker.Editor
{
    static class EventHandlers
    {
        private static EditorState unfocusControlsForState;
        public static BorderSelection resizeDirection;

        [EventHandler(-4)]
        private static void HandleFocusing(EditorInputInfo inputInfo)
        {
            EditorState state = inputInfo.editorState;
            state.focusedState = CanvasUtils.StateAtPosition(inputInfo.inputPos);
            if (unfocusControlsForState == state && Event.current.type == EventType.Repaint)
            {
                GUIUtility.hotControl = 0;
                GUIUtility.keyboardControl = 0;
                unfocusControlsForState = null;
            }
        }

        [EventHandler(EventType.MouseDown, -2)]
        private static void HandleSelecting(EditorInputInfo inputInfo)
        {
            EditorState state = inputInfo.editorState;
            if (inputInfo.inputEvent.button == 0 && state.focusedState != state.selectedState && !state.connectState)
            {
                unfocusControlsForState = state;
                state.selectedState = state.focusedState;
                StateEditorWindow.Editor.Repaint();
            }
            if (state.selectedState != null)
                UnityEditor.Selection.activeObject = state.selectedState;
        }

        [EventHandler(EventType.MouseDown, 110)]
        private static void HandleStateDraggingStart(EditorInputInfo inputInfo)
        {
            if (GUIUtility.hotControl > 0)
                return;

            EditorState state = inputInfo.editorState;
            if (!state.connectState && inputInfo.inputEvent.button == 0 && !ReferenceEquals(state.focusedState, null) && state.focusedState == state.selectedState)
            { 
                state.dragState = true;
                state.dragMouseStart = inputInfo.inputPos;
                state.dragObjectStart = state.focusedState.Rect.position;
                state.dragOffset = Vector2.zero;
                inputInfo.inputEvent.delta = Vector2.zero;
            }
        }

        [EventHandler(EventType.MouseDown, 104)]
        private static void HandleStateResizeStart(EditorInputInfo inputInfo)
        {
            if (GUIUtility.hotControl > 0)
                return;
            var state = inputInfo.editorState;
            if (!state.connectState && !state.dragState && inputInfo.inputEvent.button == 0)
            {
                var focusedState = CanvasUtils.StateAtPosition(inputInfo.inputPos);
                state.focusedState = focusedState;
                if (focusedState != null)
                {
                    if (CanvasUtils.CheckBorderSelection(focusedState, inputInfo.inputPos, out resizeDirection))
                    {
                        state.focusedState = focusedState;
                        var startSizePos = Vector2.zero;
                        if ((resizeDirection & BorderSelection.Left) != 0)
                            startSizePos.x = focusedState.Rect.xMin;
                        else if ((resizeDirection & BorderSelection.Right) != 0)
                            startSizePos.x = focusedState.Rect.xMax;
                        if ((resizeDirection & BorderSelection.Top) != 0)
                            startSizePos.y = focusedState.Rect.yMin;
                        else if ((resizeDirection & BorderSelection.Bottom) != 0)
                            startSizePos.y = focusedState.Rect.yMax;

                        state.resizingState = true;
                        state.StartDrag(inputInfo.inputPos, startSizePos);
                        inputInfo.inputEvent.Use();
                    }
                }
            }
        }



        [EventHandler(EventType.MouseDrag)]
        private static void HandleStateDragging(EditorInputInfo inputInfo)
        {
            EditorState state = inputInfo.editorState;
            if (state.selectedState != null && GUIUtility.hotControl == 0)
            {
                state.dragOffset = inputInfo.inputPos - state.dragMouseStart;
                var newPosition = state.dragObjectStart + state.dragOffset;
                if (state.dragState)
                {
                    if (StateMachineRenderer.CanDrag(state.selectedState, newPosition))
                        state.selectedState.Rect.position = state.dragObjectStart + state.dragOffset;
                }
                else if (state.resizingState)
                {
                    var selectedState = state.selectedState;
                    var newSizePos = state.dragObjectPos;
                    Rect r = selectedState.Rect;
                    if ((resizeDirection & BorderSelection.Left) != 0)
                        selectedState.Rect.xMin = r.xMax - Math.Max(StateMachineRenderer.GetMinStateWidth(selectedState), r.xMax - newSizePos.x);
                    else if ((resizeDirection & BorderSelection.Right) != 0)
                        selectedState.Rect.xMax = r.xMin + Math.Max(StateMachineRenderer.GetMinStateWidth(selectedState), newSizePos.x - r.xMin);

                    if ((resizeDirection & BorderSelection.Top) != 0)
                        selectedState.Rect.yMin = r.yMax - Math.Max(StateMachineRenderer.GetMinStateHeight(selectedState), r.yMax - newSizePos.y);
                    else if ((resizeDirection & BorderSelection.Bottom) != 0)
                        selectedState.Rect.yMax = r.yMin + Math.Max(StateMachineRenderer.GetMinStateHeight(selectedState), newSizePos.y - r.yMin);
                    if (selectedState.HasParent)
                    {
                        var nestedRect = StateMachineRenderer.GetNestedRect(selectedState.ParentState);
                        GeomUtils.ClipRect(ref selectedState.Rect, nestedRect);
                    }
                }
                inputInfo.inputEvent.Use();
                StateEditorWindow.Editor.Repaint();
            }
        }

        [EventHandler(EventType.MouseDown)]
        [EventHandler(EventType.MouseUp)]
        private static void HandleStateDraggingEnd(EditorInputInfo inputInfo)
        {
            inputInfo.editorState.EndDrag();
            inputInfo.editorState.dragState = false;
            inputInfo.editorState.resizingState = false;
            StateEditorWindow.Editor.Repaint();
            GUI.changed = true;
        }

        [EventHandler(EventType.MouseDown, 105)]
        private static void HandleCanvasPanningStart(EditorInputInfo inputInfo)
        {
            if (GUIUtility.hotControl > 0)
                return;
            var state = inputInfo.editorState;
            if ((state.focusedState == null) && (inputInfo.inputEvent.button == 0 || inputInfo.inputEvent.button == 2))
            {
                state.panningState = true;
                state.StartDrag(inputInfo.inputPos, state.panOffset);
            }
        }

        [EventHandler(EventType.MouseDrag)]
        private static void HandleCanvasPanning(EditorInputInfo inputInfo)
        {
            var state = inputInfo.editorState;
            if (state.panningState)
            {
                state.panOffset += state.UpdateDrag(inputInfo.inputPos);
                StateEditorWindow.Editor.Repaint();
            }
        }

        [EventHandler(EventType.MouseUp)]
        private static void HandleCanvasPanningEnd(EditorInputInfo inputInfo)
        {
            if (inputInfo.editorState.panningState)
                inputInfo.editorState.panOffset = inputInfo.editorState.EndDrag();
            inputInfo.editorState.panningState = false;
        }

        [EventHandler(EventType.ScrollWheel)]
        private static void HandleZooming(EditorInputInfo inputInfo)
        {
            inputInfo.editorState.zoom = (float) Math.Round(
                Math.Min(4.0, Math.Max(0.6, inputInfo.editorState.zoom + inputInfo.inputEvent.delta.y / 15)), 2);
            StateEditorWindow.Editor.Repaint();
            inputInfo.inputEvent.Use();
        }

        private static void FillContextMenu(EditorInputInfo inputInfo, GenericMenu contextMenu, ContextType contextType)
        {
            if (InputSystem.contextEntries != null)
            {
                foreach (KeyValuePair<ContextEntryAttribute, PopupMenu.MenuFunctionData> contextEntry in InputSystem.contextEntries)
                {
                    if (contextEntry.Key.contextType == contextType)
                        contextMenu.AddItem(new GUIContent(contextEntry.Key.contextPath), false, contextEntry.Value, inputInfo);
                }
            }

            object[] fillerParams = new object[] { inputInfo, contextMenu };
            if (InputSystem.contextFillers != null)
            {
                foreach (KeyValuePair<ContextFillerAttribute, Delegate> contextFiller in InputSystem.contextFillers)
                {
                    if (contextFiller.Key.contextType == contextType)
                        contextFiller.Value.DynamicInvoke(fillerParams);
                }
            }
        }

        [EventHandler(EventType.MouseDown, 0)]
        private static void HandleContextClicks(EditorInputInfo inputInfo)
        {
            if (Event.current.button == 1)
            {
                GenericMenu contextMenu = new GenericMenu();
                if (inputInfo.editorState.focusedState != null)
                    FillContextMenu(inputInfo, contextMenu, ContextType.State);
                else
                    FillContextMenu(inputInfo, contextMenu, ContextType.Canvas);
                contextMenu.Show(inputInfo.inputPos);
                Event.current.Use();
            }
        }

        [ContextEntry(ContextType.Canvas, "Create state")]
        private static void AddGlobalState(EditorInputInfo inputInfo)
        {
            StateEditorGUI.AddState(inputInfo.inputPos);
            GUI.changed = true;
        }

        [ContextEntry(ContextType.State, "Add nested state")]
        private static void AddNestedState(EditorInputInfo inputInfo)
        {
            if (inputInfo.editorState.focusedState != null)
            {
                StateEditorGUI.AddNestedState(inputInfo.editorState.focusedState, inputInfo.inputPos);
                inputInfo.inputEvent.Use();
                GUI.changed = true;
            }
        }

        [ContextEntry(ContextType.Canvas, "New state machine")]
        private static void CreateNewStateMachine(EditorInputInfo inputInfo)
        {
            StateEditorGUI.NewStateMachine();
            inputInfo.inputEvent.Use();
            GUI.changed = true;
        }

        [ContextEntry(ContextType.Canvas, "Example state machine")]
        private static void CreateStateMachineExample(EditorInputInfo inputInfo)
        {
            StateEditorGUI.ExampleStateMachine();
            inputInfo.inputEvent.Use();
            GUI.changed = true;
        }

        [ContextEntryAttribute(ContextType.State, "Delete state")]
        private static void DeleteState(EditorInputInfo inputInfo)
        {
            if (inputInfo.editorState.focusedState != null)
            {
                StateEditorGUI.DeleteStateClick(inputInfo.editorState.focusedState);
                inputInfo.inputEvent.Use();
                GUI.changed = true;
            }
        }
    }
}
