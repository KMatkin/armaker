﻿using System;
using ARMaker.Editor.Utils;
using ARMaker.Models;
using ARMaker.Scripts;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;
using EventArgs = System.EventArgs;

namespace ARMaker.Editor
{
    static class ContextMenuController
    {
        public static void Setup()
        {
            StateEditorGUI.DeleteStateClicked -= DeleteState_Clicked;
            StateEditorGUI.DeleteStateClicked += DeleteState_Clicked;
            StateEditorGUI.NewNestedStateClicked -= NewNestedState_Clicked;
            StateEditorGUI.NewNestedStateClicked += NewNestedState_Clicked;
            StateEditorGUI.NewStateClicked -= NewStateClicked;
            StateEditorGUI.NewStateClicked += NewStateClicked;
            StateEditorGUI.NewStateMachineClicked -= NewStateMachineClicked;
            StateEditorGUI.NewStateMachineClicked += NewStateMachineClicked;
            StateEditorGUI.ExampleStateMachineClicked -= ExampleStateMachine_Clicked;
            StateEditorGUI.ExampleStateMachineClicked += ExampleStateMachine_Clicked;
        }

        private static void ExampleStateMachine_Clicked(object sender, EventArgs eventArgs)
        {
            var stateMachine = StateController.GetStateMachine();
            if (stateMachine != null)
                stateMachine.Destroy();
            var stateMachineComponent = StateController.GetStateMachineComponent();
            stateMachineComponent.StateMachine = StateMachineComponent.CreateStateMachineExample();
        }

        private static void DeleteState_Clicked(ComplexState state)
        {
            StateController.GetStateMachine().RemoveState(state);
        }

        private static void NewNestedState_Clicked(ComplexState state, Vector2 mousePos)
        {
            state.AddChild(ComplexState.Create("State", new Rect(0, 20, StateMachineRenderer.MinStateWidth, StateMachineRenderer.MinStateHeight), state));
        }

        private static void NewStateClicked(Vector2 mousePos)
        {
            var stateMachine = StateController.GetStateMachine();
            if (stateMachine == null)
            {
                StateController.GetStateMachineComponent().CreateStateMachine();
                stateMachine = StateController.GetStateMachine();
            }
            int statesCount = stateMachine.StateCount;
            var newState = ComplexState.Create(
                string.Format("State {0}", statesCount + 1),
                new Rect(mousePos, new Vector2(200, 100)).RemoveCanvasOffsets());

            stateMachine.AddState(newState);
        }

        private static void NewStateMachineClicked(object sender, EventArgs args)
        {
            var stateMachine = StateController.GetStateMachine();
            if (stateMachine != null)
                stateMachine.Destroy();
            StateController.GetStateMachineComponent().CreateStateMachine();
        }
    }
}
