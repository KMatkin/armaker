﻿using System;
using System.Collections.Generic;
using System.Linq;
using ARMaker.Editor.Utils;
using ARMaker.Models;
using ARMaker.Scripts;
using UnityEditor;
using UnityEngine;
using Event = ARMaker.Models.Event;

namespace ARMaker.Editor.Inspectors
{
    [CustomEditor(typeof(ComplexState), isFallback = true)]
    public class StateInspector : UnityEditor.Editor
    {
        public new ComplexState target;
        private List<Type> eventTypes;
        private string[] eventTypeNames;
        private bool showEventSelection = false;
        private bool showEvents = false;
        private int index = 0;

        private void OnEnable()
        {
            target = (ComplexState) base.target;
            eventTypes = StateController.GetEventComponent().EventTypes;
            eventTypeNames = eventTypes.Select(t => t.Name).ToArray();
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            var stateName = EditorGUILayout.TextField("Name", target.Name);
            if (!string.IsNullOrEmpty(stateName))
            {
                target.Name = stateName;
            }
            showEvents =  EditorGUILayout.Foldout(showEvents, "Events", true);
            if (showEvents)
            {
                EditorGUI.indentLevel++;
                if (target.Events != null)
                {
                    for (var i = 0; i < target.Events.Count; i++)
                    {
                        var eventObj = target.Events[i];
                        var editor = UnityEditor.Editor.CreateEditor(eventObj);
                        editor.DrawDefaultInspector();
                        if (GUILayout.Button("Delete event", EditorStyles.miniButton))
                        {
                            DeleteEventFromState(eventObj);
                        }
                        GUILayout.Space(10);
                    }
                }
                EditorGUI.indentLevel--;
            }
            showEventSelection = EditorGUILayout.Foldout(showEventSelection, "Add event", true);
            if (showEventSelection)
            {
                GUILayout.BeginHorizontal();
                index = EditorGUILayout.Popup(index, eventTypeNames);
                if (GUILayout.Button("Add", EditorStyles.miniButton))
                {
                    Type selectedEventType = eventTypes[index];
                    AddEventToState(selectedEventType);
                }
                GUILayout.EndHorizontal();
            }
            EditorGUILayout.PropertyField(serializedObject.FindProperty("onEnter"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("onExit"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("stateUpdate"), new GUIContent("On Update"));

            if (GUI.changed)
            {
                OnModified();
            }
            serializedObject.ApplyModifiedProperties();
        }

        private void DeleteEventFromState(Event eventObj)
        {
            StateController.GetStateMachine().DeleteEventState(target, eventObj);
        }

        private void AddEventToState(Type selectedEventType)
        {
            var creationMethod = selectedEventType.GetMethods()
                .FirstOrDefault(m => m.Name == "Create" && m.GetParameters().Length == 0);
            if (creationMethod == null)
            {
                Debug.LogError(String.Format("Class {0} doesn't have method Create with 0 parameters", selectedEventType.Name));
            }
            else
            {
                var eventInstance = (Event)creationMethod.Invoke(null, null);
                target.AddEvent(eventInstance);
            }
        }

        private void OnModified()
        {
            if (StateEditorWindow.Editor != null)
                StateEditorWindow.Editor.Repaint();
            EditorSceneManagerWrapper.MakeCurrentSceneDirty();
        }
    }
}
