﻿using ARMaker.Scripts;
using Assets.ARMaker.Scripts;
using UnityEditor;
using UnityEngine;

namespace ARMaker.Editor.Inspectors
{
    [CustomEditor(typeof(EventComponent))]
    public class EventComponentInspector : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            if (GUILayout.Button("Reload event types"))
            {
                StateController.GetEventComponent().LoadEventTypes();
            }
        }
    }
}
