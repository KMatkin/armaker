﻿using System;
using System.Linq;
using ARMaker.Editor.Utils;
using ARMaker.Models;
using ARMaker.Utils;
using UnityEngine;
using EventArgs = System.EventArgs;

namespace ARMaker.Editor
{
    static class StateEditorGUI
    {
        public delegate void StateEvent(ComplexState state);
        public delegate void ConnectionEvent(ConnectionEventArgs args);
        public delegate void MousePosEvent(Vector2 mousePos);
        public delegate void StatePosEvent(ComplexState state, Vector2 pos);

        public static event MousePosEvent NewStateClicked;
        public static event StatePosEvent NewNestedStateClicked;
        public static event EventHandler NewStateMachineClicked;
        public static event EventHandler ExampleStateMachineClicked;
        public static event StateEvent DeleteStateClicked;
        public static event ConnectionEvent ConnectionClicked;
        public static event ConnectionEvent EventClicked;
        

        public static void StartGUI()
        {
            OverlayGUI.StartOverlayGUI();
        }

        public static void EndGUI()
        {
            OverlayGUI.EndOverlayGUI();
        }

        internal static void DrawCanvas(StateMachine stateMachine)
        {
            if (stateMachine != null && stateMachine.StateCount != 0)
            {
                foreach (var state in stateMachine.States.OrderBy(s => s.ZIndex))
                {
                    if (state != null)
                        StateMachineRenderer.Render(state);
                }
                if (stateMachine.Transitions != null)
                {
                    foreach (var transition in stateMachine.Transitions)
                    {
                        StateMachineRenderer.Render(transition);
                    }
                }
            }
        }

        internal static void DrawStartState(StateMachine stateMachine)
        {
            StateMachineRenderer.Render(stateMachine.Start);
        }

        public static void DeleteStateClick(ComplexState state)
        {
            if (DeleteStateClicked != null)
                DeleteStateClicked.Invoke(state);
        }

        public static void StateInputClicked(ComplexState state)
        {
            if (ConnectionClicked != null)
                ConnectionClicked.Invoke(new ConnectionEventArgs(state));
        }

        public static void EventButtonClicked(BaseState state, Models.Event eventObj)
        {
            if (EventClicked != null)
                EventClicked.Invoke(new ConnectionEventArgs(state, eventObj));
        }

        public static void DrawConnectionCurve()
        {
            var eventRect = StateMachineRenderer.GetEventRect(ConnectController.currentState,
                ConnectController.currentEvent);
            if (UnityEngine.Event.current != null)
            {
                var mousePos = UnityEngine.Event.current.mousePosition;
                var rect = new Rect(mousePos, new Vector2(1, 1));
                if (eventRect.center.x < rect.center.x)
                    DrawUtils.DrawNodeCurve(eventRect, rect);
                else
                    DrawUtils.DrawNodeCurve(rect, eventRect);
            }
        }

        public static void AddNestedState(ComplexState focusedState, Vector2 mousePos)
        {
            if (NewNestedStateClicked != null)
                NewNestedStateClicked.Invoke(focusedState, mousePos);
        }

        public static void AddState(Vector2 mousePos)
        {
            if (NewStateClicked != null)
                NewStateClicked.Invoke(mousePos);
        }

        public static void NewStateMachine()
        {
            if (NewStateMachineClicked != null)
                NewStateMachineClicked.Invoke(null, EventArgs.Empty);
        }

        public static void ExampleStateMachine()
        {
            if (ExampleStateMachineClicked != null)
                ExampleStateMachineClicked.Invoke(null, EventArgs.Empty);
        }
    }
}
