﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ARMaker.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class HideEventTypeAttribute : Attribute
    {
        public HideEventTypeAttribute()
        {
        }
    }
}
