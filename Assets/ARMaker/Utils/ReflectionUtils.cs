﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Assets.ARMaker.Utils
{
    static class ReflectionUtils
    {
        public static IEnumerable<Type> GetAllSubclassOf(Type parent)
        {
            foreach (var a in AppDomain.CurrentDomain.GetAssemblies())
                foreach (var t in a.GetTypes())
                    if (t.IsSubclassOf(parent)) yield return t;
        }

        public static IEnumerable<Type> FilterAttributes(this IEnumerable<Type> types, IEnumerable<Type> attributeTypes)
        {
            foreach (var type in types)
            {
                bool filter = true;
                foreach (var customAttribute in type.GetCustomAttributes(true))
                {
                    foreach (var filterAttributeType in attributeTypes)
                    {
                        if (customAttribute.GetType() == filterAttributeType)
                        {
                            filter = false;
                            break;
                        }
                    }
                    if (!filter)
                        break;
                }
                if (filter)
                    yield return type;
            }
        }
    }
}
