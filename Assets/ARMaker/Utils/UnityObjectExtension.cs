﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ARMaker.Utils
{
    public static class UnityObjectExtension
    {
        public static bool IsNull(this UnityEngine.Object aObj)
        {
            return (object)aObj == null;
        }
    }
}
