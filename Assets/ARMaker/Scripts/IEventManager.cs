﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ARMaker.Models;

namespace ARMaker.Scripts
{
    internal interface IEventManager
    {
        void Receive<T>(ARMaker.Models.EventArgs args) where T : Event;
    }
}
