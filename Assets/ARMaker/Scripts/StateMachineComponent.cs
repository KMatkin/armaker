﻿using ARMaker.Events;
using ARMaker.Models;
using UnityEngine;

namespace ARMaker
{
    public class StateMachineComponent : MonoBehaviour
    {
        [SerializeField]
        public StateMachine StateMachine = new StateMachine();

        void Awake()
        {
#if !UNITY_EDITOR
            DontDestroyOnLoad(gameObject);      
#endif
        }

        public void CreateStateMachine()
        {
            StateMachine = new StateMachine();
        }

        public static StateMachine CreateStateMachineExample()
        {
            var virtualButtonEvent = VirtualButtonClickEvent.Create("VB Click1", null);

            var stateMachine = new StateMachine();
            var stateOn = ComplexState.Create("On", new Rect(50, 50, 400, 300));
            stateOn.AddEvent(virtualButtonEvent);
            stateOn.AddEvent(VirtualButtonClickEvent.Create("VB Click2", null));
            stateOn.AddEvent(VirtualButtonClickEvent.Create("VB Click3", null));
            stateOn.AddEvent(VirtualButtonClickEvent.Create("VB Click4", null));


            var stateOff = ComplexState.Create("Off", new Rect(250, 250, 200, 200));
            stateOff.AddEvent(virtualButtonEvent);

            var nestedOn = ComplexState.Create("NestedOn", new Rect(0, 20, 200, 100), stateOn);
            stateOn.AddChild(nestedOn);
            stateMachine.AddState(stateOn);
            stateMachine.AddState(stateOff);
            stateMachine.Start = StartState.Create(new Rect(0, 50, 70, 20));

            var fromOnToOff = Transition.Create(stateOn, stateOff, virtualButtonEvent);
            var fromOffToOn = Transition.Create(stateOff, stateOn, virtualButtonEvent);
            stateMachine.AddTransition(fromOnToOff);
            stateMachine.AddTransition(fromOffToOn);
            return stateMachine;
        }

        public void CreateStartState()
        {
            StateMachine.Start = StartState.Create(new Rect(0, 50, 70, 20));
        }

        public void AddState(ComplexState newState)
        {
            StateMachine.AddState(newState);
        }

        public void SetupStartState()
        {
            if (ReferenceEquals(StateMachine.Start, null))
            {
                CreateStartState();
            }
        }
    }
}
