﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using ARMaker.Attributes;
using ARMaker.Models;
using ARMaker.Scripts;
using Assets.ARMaker.Utils;
using UnityEngine;
using Event = ARMaker.Models.Event;
using EventArgs = ARMaker.Models.EventArgs;

namespace Assets.ARMaker.Scripts
{
    public class EventComponent : MonoBehaviour, IEventManager
    {
        private List<Type> _eventTypes;
        private StateMachine _stateMachine;

        private void Awake()
        {
            StateMachine.currentState = StateMachine.Start;
            MakeTransitionsFromStart();
        }

        private void Update()
        {
            if (_stateMachine.currentState is ComplexState)
                ((ComplexState)_stateMachine.currentState).stateUpdate.Invoke();
        }

        private void MakeTransitionsFromStart()
        {
            if (StateMachine.currentState is StartState)
            {
                Receive<EmptyEvent>(null);
            }
        }

        public List<Type> EventTypes
        {
            get
            {
                if (_eventTypes == null)
                {
                    LoadEventTypes();
                }
                return _eventTypes;
            }
        }

        private StateMachine StateMachine
        {
            get { return _stateMachine ?? (_stateMachine = StateController.GetStateMachine()); }
        }

        public void LoadEventTypes()
        {
            _eventTypes = ReflectionUtils.GetAllSubclassOf(typeof(Event))
                .FilterAttributes(new List<Type>() {typeof(HideEventTypeAttribute)}).ToList();
        }

        public void Receive<T>(EventArgs args) where T : Event
        {
            Debug.Log("Receive event " + typeof(T).Name);
            var newState = CheckTransition(_stateMachine.currentState, typeof(T), args);
            if (NeedToChangeCurrentState(newState))
            {
                MakeTransition(_stateMachine.currentState, newState);
            }
        }

        private void MakeTransition(BaseState oldState, BaseState newState)
        {
            Debug.Log(String.Format("Making transition from {0} to {1}", oldState.Name, newState.Name));
            if (oldState is ComplexState)
                ExecuteActions((ComplexState)oldState, false);
            if (newState is ComplexState)
                ExecuteActions((ComplexState)newState, true);
            StateMachine.currentState = GetCurrentState(newState);
            if (StateMachine.currentState is StartState)
            {
                MakeTransitionsFromStart();
            }
        }

        private BaseState GetCurrentState(BaseState newState)
        {
            ComplexState complexState = (ComplexState) newState;
            if (complexState == null)
                return newState;
            else
                return complexState.IsSimple ? (BaseState)complexState : complexState.StartState;
        }

        private void ExecuteActions(ComplexState state, bool isEnter)
        {
            if (state == null)
                return;
            if (isEnter)
                state.onEnter.Invoke();
            else
            {
                state.onExit.Invoke();
                ExecuteActions(state.ParentState, false);
            }
        }

        private bool NeedToChangeCurrentState(BaseState newState)
        {
            return newState != null;
        }

        private BaseState CheckTransition(BaseState state, Type eventType, EventArgs args)
        {
            if (state == null || eventType == null || !eventType.IsSubclassOf(typeof(Event)))
                return null;

            var transitionsFromState = StateMachine
                .TransitionsFromState(state);

            
            var transition = transitionsFromState
                .Where(trans => trans.Event.GetType() == eventType)
                .FirstOrDefault(trans => trans.Event.ArgsEquals(args));
            if (transition != null)
                return transition.NewState;
            else
            {
                ComplexState complexState = (ComplexState) state;
                if (complexState != null)
                {
                    return CheckTransition(complexState.ParentState, eventType, args);
                }
            }
            return null;
        }
        
    }
}
