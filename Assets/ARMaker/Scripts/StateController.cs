﻿using ARMaker.Models;
using Assets.ARMaker.Scripts;
using UnityEngine;

namespace ARMaker.Scripts
{
    public static class StateController
    {
        public static GameObject GetStateController()
        {
            return GameObject.Find("StateController");
        }

        public static StateMachineComponent GetStateMachineComponent()
        {
            var stateController = GetStateController();
            return stateController != null ? stateController.GetComponent<StateMachineComponent>() : null;
        }

        public static EventComponent GetEventComponent()
        {
            var stateController = GetStateController();
            return stateController != null ? stateController.GetComponent<EventComponent>() : null;
        }

        public static StateMachine GetStateMachine()
        {
            var stateMachineComponent = GetStateMachineComponent();
            return stateMachineComponent != null ? stateMachineComponent.StateMachine : null;
        }
    }
}
