﻿using System;
using UnityEngine;
using Event = ARMaker.Models.Event;
using EventArgs = ARMaker.Models.EventArgs;

namespace ARMaker.Events
{
    public class VirtualButtonClickEvent : Event
    {
        public new VirtualButtonClickEventArgs Args;

        public static VirtualButtonClickEvent Create(string name, GameObject button)
        {
            var virtualButtonClickEvent = ScriptableObject.CreateInstance<VirtualButtonClickEvent>();
            virtualButtonClickEvent.Name = name;
            virtualButtonClickEvent.Args = new VirtualButtonClickEventArgs(button);
            return virtualButtonClickEvent;
        }

        public static VirtualButtonClickEvent Create()
        {
            var virtualButtonClickEvent = ScriptableObject.CreateInstance<VirtualButtonClickEvent>();
            virtualButtonClickEvent.Name = "VBClick";
            return virtualButtonClickEvent;
        }

        public override bool ArgsEquals(EventArgs args)
        {
            var vbArgs = args as VirtualButtonClickEventArgs;
            if (vbArgs == null)
                return false;
            return vbArgs.Button == Args.Button;
        }
    }

    [Serializable]
    public class VirtualButtonClickEventArgs : EventArgs
    {
        [SerializeField]
        public GameObject Button;

        public VirtualButtonClickEventArgs(GameObject button)
        {
            Button = button;
        }
    }
}
