﻿using System;
using UnityEngine;
using Event = ARMaker.Models.Event;
using EventArgs = ARMaker.Models.EventArgs;

namespace ARMaker.Events
{
    public class ImageTargetLostEvent : Event
    {
        public new ImageTargetLostEventArgs Args;

        public static ImageTargetLostEvent Create(string name, string targetName)
        {
            var newEvent = CreateInstance<ImageTargetLostEvent>();
            newEvent.Name = name;
            newEvent.Args = new ImageTargetLostEventArgs(targetName);
            return newEvent;
        }

        public static ImageTargetLostEvent Create()
        {
            var newEvent = ScriptableObject.CreateInstance<ImageTargetLostEvent>();
            newEvent.Name = "ITLost";
            return newEvent;
        }

        public override bool ArgsEquals(EventArgs args)
        {
            var itArgs = args as ImageTargetLostEventArgs;
            if (itArgs == null)
                return false;
            return itArgs.targetName.Equals(Args.targetName);
        }
    }

    [Serializable]
    public class ImageTargetLostEventArgs : EventArgs
    {
        public string targetName;

        public ImageTargetLostEventArgs(string targetName)
        {
            this.targetName = targetName;
        }
    }
}
