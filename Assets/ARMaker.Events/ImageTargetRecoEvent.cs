﻿using System;
using UnityEngine;
using Event = ARMaker.Models.Event;
using EventArgs = ARMaker.Models.EventArgs;


namespace ARMaker.Events
{
    public class ImageTargetRecoEvent : Event
    {
        public new ImageTargetRecoEventArgs Args;

        public static ImageTargetRecoEvent Create(string name, string targetName)
        {
            var newEvent = ScriptableObject.CreateInstance<ImageTargetRecoEvent>();
            newEvent.Name = name;
            newEvent.Args = new ImageTargetRecoEventArgs(targetName);
            return newEvent;
        }

        public static ImageTargetRecoEvent Create()
        {
            var newEvent = ScriptableObject.CreateInstance<ImageTargetRecoEvent>();
            newEvent.Name = "ITReco";
            return newEvent;
        }

        public override bool ArgsEquals(EventArgs args)
        {
            var itArgs = args as ImageTargetRecoEventArgs;
            if (itArgs == null)
                return false;
            return itArgs.targetName.Equals(Args.targetName);
        }
    }

    [Serializable]
    public class ImageTargetRecoEventArgs : EventArgs
    {
        public string targetName;

        public ImageTargetRecoEventArgs(string targetName)
        {
            this.targetName = targetName;
        }
    }
}
