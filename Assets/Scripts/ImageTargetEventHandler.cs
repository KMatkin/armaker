﻿using System.Collections;
using System.Collections.Generic;
using ARMaker.Events;
using ARMaker.Models;
using ARMaker.Scripts;
using Assets.ARMaker.Scripts;
using UnityEngine;
using Vuforia;

public class ImageTargetEventHandler : MonoBehaviour, ITrackableEventHandler {
	
	private TrackableBehaviour mTrackableBehaviour;
    private EventComponent eventComponent;

	void Start() {
        mTrackableBehaviour = GetComponent<TrackableBehaviour>();
        if (mTrackableBehaviour)
        {
            mTrackableBehaviour.RegisterTrackableEventHandler(this);
        }
	    eventComponent = StateController.GetEventComponent();
	}

	public void OnTrackableStateChanged (TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
	{
	    if (eventComponent != null)
	    {
	        if (newStatus == TrackableBehaviour.Status.TRACKED || 
                newStatus == TrackableBehaviour.Status.DETECTED || 
                newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
	        {
	            eventComponent.Receive<ImageTargetRecoEvent>(
	                new ImageTargetRecoEventArgs(mTrackableBehaviour.TrackableName));
	        }
	        else
	        {
	            eventComponent.Receive<ImageTargetLostEvent>(
	                new ImageTargetLostEventArgs(mTrackableBehaviour.TrackableName));
	        }
	    }
	}
}
