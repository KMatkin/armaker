﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CoverBehaviour : MonoBehaviour
{
    public GameObject[] screws;
    public GameObject screwDriver;
    public GameObject coverPlane;
    private float startScrewDriverY;
    private float startScrewY;

	void Start ()
	{
	    startScrewDriverY = screwDriver.transform.position.y;
	    if (screws != null && screws.Any())
	        startScrewY = screws[0].transform.position.y;
        HideAllTools();
        coverPlane.SetActive(false);
	}

    public void Unscrew(int index)
    {
        if (screws == null || screws.Length == 0)
            return;
        TransformDriverToScrew(index);
        HideAllTools();
        ShowTools(index, true);
        StartCoroutine(UnscrewRoutine(index));
    }

    public void ReleaseCover()
    {
        coverPlane.SetActive(true);
        StartCoroutine(ReleaseCoverRoutine());
    }

    private IEnumerator ReleaseCoverRoutine()
    {
        yield return SmoothMove(new Vector3(0, 1, 0), 1f,
            new List<Transform> {coverPlane.transform});
        coverPlane.SetActive(false);
    }

    private void TransformDriverToScrew(int index)
    {
        var screwPosition = screws[index].transform.position;
        screwDriver.transform.position = new Vector3(screwPosition.x, startScrewDriverY,
            screwPosition.z);
        screws[index].transform.position = new Vector3(screwPosition.x, startScrewY, screwPosition.z);
    }

    public void ShowTools(int index)
    {
        ShowTools(index, true);
    }

    public void HideTools(int index)
    {
        ShowTools(index, false);
    }

    public void HideAllTools()
    {
        for (var i = 0; i < screws.Length; i++)
        {
            ShowTools(i, false);
        }
    }

    private void ShowTools(int index, bool value)
    {
        screws[index].SetActive(value);
        screwDriver.SetActive(value);
    }

    private IEnumerator UnscrewRoutine(int index)
    {
        GameObject screw = screws[index];
        int timesToRotate = 3;
        for (int i = 0; i < timesToRotate; i++)
        {
            for (int j = 0; j < 360; j += 5)
            {
                screwDriver.transform.Rotate(-Vector3.right, j * Time.deltaTime);
                screw.transform.Rotate(-Vector3.up, j * Time.deltaTime);
                yield return null;
            }
        }
        yield return new WaitForSeconds(0.1f);

        // Fly away
        yield return SmoothMove(new Vector3(0, 1, 0), 1f,
            new List<Transform> { screw.transform, screwDriver.transform});
        ShowTools(index, false);
    }

    IEnumerator SmoothMove(Vector3 direction, float speed, IList<Transform> transforms)
    {
        float startime = Time.time;
        Vector3 startPos = Vector3.zero;
        Vector3 endPos = direction;
        var startPositions = transforms.Select(t => t.position).ToList();
        var endPositions = startPositions.Select(t => t + direction).ToList();
        while (startPos != endPos && ((Time.time - startime) * speed) < 1f)
        {
            float t = (Time.time - startime) * speed;
            for (var i = 0; i < transforms.Count; i++)
            {
                transforms[i].position = Vector3.Lerp(startPositions[i], endPositions[i], t);
            }
            yield return null;
        }
    }
}
