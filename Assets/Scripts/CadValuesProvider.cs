﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;

public class CadValuesProvider : MonoBehaviour
{
    private string baseUrl = @"https://matkink.pythonanywhere.com";

    public int cadId;
    public TextMesh temperature1;
    public TextMesh temperature2;

    public void StartUpdateValues()
    {
        StartCoroutine(UpdateValues(cadId));
    }

    private IEnumerator UpdateValues(int cadId)
    {
        string url = string.Format("{0}/cads/{1}", baseUrl, cadId);
        while (true)
        {
            WWW www = new WWW(url);
            yield return new WaitForSeconds(2f);
            if (www.error == null)
            {
                ProcessJson(www.text);
            }
            else
            {
                Debug.LogError(string.Format("Cannot load values from server ({0})", www.error));
            }
        }
    }

    private void ProcessJson(string jsonString)
    {
        var data = JSON.Parse(jsonString);
        var temp1 = data["temperature1"];
        var temp2 = data["temperature2"];

        temperature1.text = temp1.AsFloat.ToString("##.###");
        temperature2.text = temp2.AsFloat.ToString("##.###");
    }

    public void StopUpdateValues()
    {
        StopCoroutine(UpdateValues(cadId));
    }
}
