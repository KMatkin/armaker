﻿using ARMaker.Events;
using ARMaker.Scripts;
using Assets.ARMaker.Scripts;
using UnityEngine;
using Vuforia;

public class VirtualButtonEventHandler : MonoBehaviour, IVirtualButtonEventHandler
{
    private EventComponent eventComponent;

    public void OnButtonPressed(VirtualButtonAbstractBehaviour vb)
    {
        if (eventComponent != null)
            eventComponent.Receive<VirtualButtonClickEvent>(new VirtualButtonClickEventArgs(vb.gameObject));
    }

    public void OnButtonReleased(VirtualButtonAbstractBehaviour vb)
    {
        Debug.Log("ButtonReleased " + vb.VirtualButtonName);
    }
    
    void Start () {
        eventComponent = StateController.GetEventComponent();
        // Search for all Children from this ImageTarget with type VirtualButtonBehaviour
        VirtualButtonBehaviour[] vbs = GetComponentsInChildren<VirtualButtonBehaviour>();
        for (int i = 0; i < vbs.Length; ++i)
        {
            vbs[i].RegisterEventHandler(this);
        }
    }
}
